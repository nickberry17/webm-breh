//
//  SavePreferences.swift
//  Webm Breh
//
//  Created by nick on 15/02/2016.
//  Copyright © 2016 Nicholas Berry. All rights reserved.
//

import Cocoa

class SavePreferences: NSViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiKeySavedText.hidden = true
        if(defaults.valueForKey(userApiKeyConstant) != nil) {
            textFieldApiKey!.placeholderString = defaults.stringForKey(userApiKeyConstant)
        }
    }
    
    
    @IBOutlet weak var apiKeySavedText: NSTextField!
    let userApiKeyConstant = "userApiKey"
    let defaults = NSUserDefaults.standardUserDefaults()
    var apiKeyTitle = NSUserDefaults.standardUserDefaults().stringForKey("userApiKey")

    //MARK textfield for api key
    @IBOutlet weak var textFieldApiKey: NSTextField?
    
    //MARK save API Key as default preference
    @IBAction func btnSaveApiKey(sender: AnyObject) {
        defaults.setObject(self.textFieldApiKey!.stringValue, forKey: userApiKeyConstant)
        defaults.synchronize()
        apiKeySavedText.hidden = false
        
        func dialogNoKey(question: String, text: String) -> Bool {
            let alert: NSAlert = NSAlert()
            alert.messageText = question
            alert.informativeText = text
            alert.alertStyle = NSAlertStyle.WarningAlertStyle
            alert.addButtonWithTitle("OK")
            let res = alert.runModal()
            if res == NSAlertFirstButtonReturn {
                return true
            }
            return false
        }
        
        _ = dialogNoKey("A relaunch is required", text: "You'll just have to quit and reopen Webm Breh, but you should never have to do this again unless you change your API Key")
    }
    
    override func viewWillDisappear() {

    }
    

}