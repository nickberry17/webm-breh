//
//  AppDelegate.swift
//  CloudConvert Mac
//
//  Created by Nick on 3/12/2015.
//  Copyright © 2015 Nicholas Berry. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

