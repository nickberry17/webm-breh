//
//  ViewController.swift
//  CloudConvert Mac aka Webm Breh
//
//  Created by Nick on 3/12/2015.
//  Copyright © 2015 Nicholas Berry. All rights reserved.
//

import Cocoa
import CloudConvert


//MARK Load Default Preferences: API Key
var defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
var ApiKey = defaults.stringForKey("userApiKey") as String?
let blank = ""

class ViewController: NSViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activitySpinner!.hidden = true;
        if (ApiKey == nil) {
             ApiKey = blank;
                if (ApiKey == blank) {
                    statusText!.title = "An API Key is not set in the Preferences. Gotta do that first, breh."
                    btnChooseFile.enabled = false
                }
        } else {
        defaults.synchronize()
        statusText!.title = "Ready"
        }
        btnConvert.enabled = false
    }
    
    var chosenFileURL = NSURL(fileURLWithPath: blank)
    @IBOutlet weak var activitySpinner: NSProgressIndicator?
    @IBOutlet weak var statusText: NSTextFieldCell?
    
    
    @IBOutlet weak var btnChooseFile: NSButton!
    //MARK: Import button, pick a file
    @IBAction func chooseFile(sender: AnyObject) {
        let openPanel = NSOpenPanel()
        
        //utilising some NSOpenPanel bulit-in functionality
        openPanel.allowedFileTypes = ["gif"]
        /* extra openPanel functionality for later updates. uncomment to try ¡warning: untested!
        openPanel.canChooseDirectories = true
        openPanel.resolvesAliases = true
        openPanel.allowsMultipleSelection = true
        */
        openPanel.title = "Choose a gif"
        openPanel.beginWithCompletionHandler({(result:Int) in
            if(result == NSFileHandlingPanelOKButton)
            {
                //must be var because it can be changed. e.g. user chooses a file, changes their mind, and picks another before submitting the conversion job
                var fileURL = openPanel.URL!
                print(fileURL)
                self.chosenFileURL = fileURL
                //grab the file path URL object as a relative path, and unwrap it because it's an optional
                let formattedFileURL = self.chosenFileURL.relativePath!
                self.statusText!.title = "Ready to convert " + "\(formattedFileURL)"
                self.btnConvert.enabled = true
            }
        })
    }
    @IBOutlet weak var btnConvert: NSButton!
    
    @IBAction func cloudConvert(sender: AnyObject) {
        if (ApiKey != blank) && (chosenFileURL != blank) {
            
            self.activitySpinner?.hidden = false;
            self.activitySpinner?.startAnimation(self)
            self.btnChooseFile.enabled = false
            self.btnConvert.enabled = false
            
            statusText?.title = "Up, up, and away!"
            
            CloudConvert.apiKey = ApiKey!
            
            let inputURL  = chosenFileURL
            //even though this cast always succeeds, it must be done this way or the CloudConvert library can't handle it
            let outputURL = NSFileManager.defaultManager().URLsForDirectory(.PicturesDirectory, inDomains: .UserDomainMask)[0] as? NSURL
            /*set file name as chosen file name's name, ommitting the preceeding url path
            We can't make realOutputURL an NSString because CloudConvert.convert() needs it to be an NSURL.  But the chosenFileURL.lastPathComponent needs to be a string
            */
            let realOutputURL = outputURL?.URLByAppendingPathComponent("\(chosenFileURL.lastPathComponent!.stringByReplacingOccurrencesOfString(".gif", withString: "")).webm")
            
            
            CloudConvert.convert([
                "inputformat": "gif",
                "outputformat" : "webm",
                "input" : "upload",
                "file": inputURL,
                "download": realOutputURL!
                ],
                progressHandler: { (step, percent, message) -> Void in
                    print(step! + " " + percent!.description + "%: " + message!)
                    self.statusText?.title = step! + " " + percent!.description + "%: " + message!
                },
                completionHandler:
                { (path, error) -> Void in
                    if(error != nil) {
                        print("Failed: " + error!.description)
                        self.statusText?.title = "There was an error with the CloudConvert online service.  Sorry, breh." + error!.description
                        self.activitySpinner?.stopAnimation(self)
                        self.activitySpinner?.hidden = true;
                        self.btnConvert.enabled = false
                        
                        //display a nice error alert for my brehs
                        func dialogError(question: String, text: String) -> Bool {
                            let alert: NSAlert = NSAlert()
                            alert.messageText = question
                            alert.informativeText = text
                            alert.alertStyle = NSAlertStyle.WarningAlertStyle
                            alert.addButtonWithTitle("OK")
                            let res = alert.runModal()
                            if res == NSAlertFirstButtonReturn {
                                return true
                                
                            }
                            return false
                        }
                        
                        let answer = dialogError("Oh no, breh! I can't use the CloudConvert service", text: "Check that there is a valid CloudConvert API Key set in my app Preferences, and that the Internet is working.")
                        
                    } else {
                        print("Done! Output file saved to your Pictures folder")
                        self.activitySpinner?.stopAnimation(self)
                        self.activitySpinner?.hidden = true;
                        self.statusText?.title = "Done! The Webm is saved to your Pictures folder."
                        self.btnChooseFile.enabled = true
                        self.btnConvert.enabled = false
                    }
            })
        } else {
            //display an error alert to remind brehs to set the api key
            func dialogNoKey(question: String, text: String) -> Bool {
                let alert: NSAlert = NSAlert()
                alert.messageText = question
                alert.informativeText = text
                alert.alertStyle = NSAlertStyle.WarningAlertStyle
                alert.addButtonWithTitle("OK")
                let res = alert.runModal()
                if res == NSAlertFirstButtonReturn {
                    return true
                }
                return false
            }
            
            _ = dialogNoKey("I should have reminded you. Sorry, breh!", text: "There is no CloudConvert API Key set in the app Preferences.  Get one by registering at CloudConvert.com and viewing your Dashboard")
        }
        
    }
    
}
var representedObject: AnyObject? {
didSet {
    // Update the view, if already loaded.
}

}




