# Webm Breh

Webm Breh v1 is a .gif to .webm conversion tool for Mac OS X written in Swift.  
It is a frontend to the CloudConvert API.  A CloudConvert API Key is required to use this app. Obtain one at cloudconvert.com

### Version
1.0

### Download
https://bitbucket.org/nickberry17/webm-breh/downloads/

### Installation

1. Copy Webm Breh.app to your Applications folder.  
2. Register an account on cloudconvert.com and copy your API Key from your account Dashboard
3. Open Webm Breh's Preferences by using Cmd + , or going to the Webm Breh menu > Preferences
4. Save your API key
5. Restart the app, and start using!

### Trivia
+ Webm Breh is sandboxed for added security, even if your version of OS X does not sandbox apps automatically
+ There are some easter eggs

License
----

[GNU Affero GPLv3]


[breh]: <https://gitgud.io/smithnicholas17/webm-breh>
[CocoaPods]: <https://cocoapods.org/>
[CloudConvert]: <https://cloudconvert.com>
[Alamofire]: <https://github.com/Alamofire/Alamofire>
[GNU Affero GPLv3]: <https://www.gnu.org/licenses/agpl-3.0.en.html>